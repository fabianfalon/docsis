# -*- coding: utf-8 -*-
import sqlalchemy as sa

from api.core import db, BaseModel
from api.helpers import JsonSerializer
from api.models.users import User

class Turn(BaseModel, JsonSerializer):
    """ class Turn"""
    __tablename__ = 'turns'
    __json_public__ = ['id', 'name', 'description']

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    date = sa.Column(sa.String(125))
    start_hour = sa.Column(sa.String(125))
    end_hour = sa.Column(sa.String(125))

    doctor = sa.relationship(User, uselist=False)
    doctor_id = sa.Column(sa.Integer, sa.ForeignKey(User.id))
    
    patient = sa.relationship(User, uselist=False)
    patient_id = sa.Column(sa.Integer)

    status = sa.Column(sa.Integer, sa.ForeignKey(User.id))

    def save(self, commit=True):
        """ save """
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def update(self, commit=True, **kwargs):
        for attr, value in kwargs.iteritems():
            setattr(self, attr, value)
        return commit and self.save() or self

    def delete(self, commit=True):
        db.session.delete(self)
        return commit and db.session.commit()

