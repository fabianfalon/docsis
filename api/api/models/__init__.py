# -*- coding: utf-8 -*-

from .users import *
from .audit import *
from .specialty import *
from .prepaid import *

class DB(object):
    pass


container = DB()
container.User = User
# container.Role = Role
container.Audit = Audit
container.Specialty = Specialty
container.Prepaid = Prepaid

container.User.db = container
# container.Role.db = container
container.Audit.db = container
container.Specialty.db = container
container.Prepaid.db = container
