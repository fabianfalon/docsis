# -*- coding: utf-8 -*-
import datetime
import logging
from flask import request, jsonify, make_response
from flask_restful import Resource

from api.models import *
from api.settings import SECRET_KEY
import jwt


def create_token(user):
    payload = {
        'sub': user.id,
        'iat': datetime.datetime.utcnow(),
        'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1),
        'scope': user.roles
    }
    token = jwt.encode(payload, SECRET_KEY)
    return token.decode('unicode_escape')


class AuthResource(Resource):

    def post(self):

        data = request.get_json(force=True)
        email = data['email']
        password = data['password']
        user = User.query.filter_by(email=email).first()
        response = dict(data=[])
        if not user:
            response['message'] = 'El email no existe'
            response['status_code'] = 403
            response['error'] = True
            return response
        if user.check_password(password):
            token = create_token(user)
            return {'token': token}
        else:
            response['message'] = 'La contrasenia es incorrecta'
            response['status_code'] = 403
            response['error'] = True
            return response
