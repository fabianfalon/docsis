# -*- coding: utf-8 -*-
from flask import url_for, request, g, jsonify, make_response
from flask.ext.restful import reqparse, abort
from sqlalchemy import and_, or_, not_, func, CHAR, literal
from api.models import *
from api.core import db
from api.resources import Resource
import math

class SpecialtyCollection(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        super(SpecialtyCollection, self).__init__()

    def get(self):
        page = 0
        limit_row = 15
        response = dict(data=[])
        self.parser.add_argument('pages', type=str, required=False,
                                 location='args')
        self.parser.add_argument('limit', type=str, required=False,
                                 location='args')
        self.parser.add_argument('sortField', type=str, required=False,
                                 location='args')
        self.parser.add_argument('sortDirection', type=str, required=False,
                                 location='args')

        q = self.parser.parse_args()

        if q.pages:
            page = int(q.pages)

        if q.limit:
            limit_row = int(q.limit)

        query = db.session.query(
            Specialty.id,
            Specialty.name
        )

        if q.sortField == 'name':
            if q.sortDirection == 'asc':
                query = query.order_by(Specialty.name.asc())
            else:
                query = query.order_by(Specialty.name.desc())
        if q.sortField == 'id':
            if q.sortDirection == 'asc':
                query = query.order_by(Specialty.id.asc())
            else:
                query = query.order_by(Specialty.id.desc())

        else:
            query = query.order_by(Specialty.id.asc())

        query = query.limit(limit_row).offset(int(page) * limit_row).all()
        response['data'] = query
        response['total'] = Specialty.query.count()
        response['pages'] = int(
            math.ceil(Specialty.query.count() / float(limit_row))
        )
        return response

