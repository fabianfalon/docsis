# -*- coding: utf-8 -*-
from flask import url_for, request, g, jsonify, make_response
from flask.ext.restful import reqparse, abort
from sqlalchemy import and_, or_, not_, func, CHAR, literal
from api.models import *
from api.core import db
from api.resources import Resource
import math
import datetime
import json, logging

class DoctorCollection(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        super(DoctorCollection, self).__init__()

    def get(self):
        page = 0
        limit_row = 15
        response = dict(data=[])
        self.parser.add_argument('pages', type=str, required=False,
                                 location='args')
        self.parser.add_argument('limit', type=str, required=False,
                                 location='args')
        self.parser.add_argument('sortField', type=str, required=False,
                                 location='args')
        self.parser.add_argument('sortDirection', type=str, required=False,
                                 location='args')

        q = self.parser.parse_args()

        if q.pages:
            page = int(q.pages)

        if q.limit:
            limit_row = int(q.limit)

        query = db.session.query(
            Doctor.id,
            Doctor.specialty_id,
            Doctor.enrollment,
            User.first_name,
            User.last_name,
            User.email,
            User.id,
        ).select_from(
            Doctor
        )

        if q.sortField == 'id':
            if q.sortDirection == 'asc':
                query = query.order_by(Doctor.id.asc())
            else:
                query = query.order_by(Doctor.id.desc())

        else:
            query = query.order_by(Doctor.id.asc())

        query = query.limit(limit_row).offset(int(page) * limit_row).all()
        response['data'] = query
        response['total'] = Doctor.query.count()
        response['pages'] = int(
            math.ceil(Doctor.query.count() / float(limit_row))
        )
        return response


class DoctorResource(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        super(DoctorResource, self).__init__()

    def post(self):
        response = dict()
        data = request.form
        doc = Doctor()
        doc.first_name = data.get('first_name')
        doc.last_name = data.get('last_name')
        doc.email= data.get('email')
        doc.specialty_id = data.get('specialty_id')
        doc.enrollment = data.get('enrollment')
        doc.address = data.get('address')
        doc.cellphone = data.get('cellphone')
        doc.password = get_hmac(data.get('password'))
        doc.active = True
        doc.registered_at = datetime.datetime.now()
        doc.confirmed_at = datetime.datetime.now()

        db.session.add(doc)
        db.session.commit()
        after = json.dumps(doc.to_json())
        response['message'] = 'User has been successfully modified'
        response['status_code'] = 200
        response['data'] = [doc.to_json()]
        logging.warning("""User has been successfully modified""")
        
        # Audit
        a = Audit(before='', after=after,
                  user_id=g.user_id,
                  action='c', object_type='doctor',
                  object_id=doc.id)
        db.session.add(a)
        db.session.commit()
        return response


class DoctorDetailResource(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        super(DoctorDetailResource, self).__init__()

    def delete(self, user_id):
        response = dict()
        try:
            doc = Doctor.query.get(user_id)
            
            # Audit
            a = Audit(before=json.dumps(doc.to_json()), after='',
                      user_id=g.user_id,
                      action='d', object_type='doctor',
                      object_id=doc.id)
            db.session.add(a)
            doc.delete()
            db.session.commit()
            response['message'] = 'User has been successfully deleted'
            response['status_code'] = 202
        except Exception as error:
            response['message'] = 'Error: {}'.format(error)
            response['status_code'] = 400

        return response

    def get(self, user_id):

        response = dict()
        query = db.session.query(
            Doctor.id,
            Doctor.specialty_id,
            Doctor.enrollment,
            User.first_name,
            User.last_name,
            User.email,
            User.id,
        ).select_from(
            Doctor
        ).filter(
            User.id == user_id
        )
        query = query.all()
        response['status_code'] = 200
        response['data'] = query
        return response

    def put(self, user_id):
        pass
