# -*- coding: utf-8 -*-
from .doctor import *

resources = [
    ('doctors', DoctorCollection, "doctor.lists"),
    ('doctors', DoctorResource, "doctor.registration"),
    ('doctors/<int:user_id>', DoctorDetailResource, "doctor.detail")

]
