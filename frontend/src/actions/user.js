import * as axios from 'axios';
import cookie from 'react-cookies';
import config from '../config';

export function loadUserData() {
  return async dispatch => {
    const token = cookie.load('token');
    axios
      .get(`http://${config.apiHost}:${config.apiPort}/api/v1/users/me`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => {
        dispatch({
          type: 'LOAD_USER_SUCCESS',
          payload: res.data
        });
      })
      .catch(err => {
        dispatch({
          type: 'LOAD_USER_FAIL',
          payload: err
        });
      });
  };
}

export default {
  loadUserData,
};