import * as axios from 'axios';
import cookie from 'react-cookies';
import config from '../config';
import { history } from '../helpers';

export function login(email, password) {
  return async (dispatch) => {
    axios.post(`http://${config.apiHost}:${config.apiPort}/api/v1/auth`, {
      email,
      password
    })
    .then(res => {
      if (res.data.token && res.data.token !== 'undefined') {
        cookie.save('token', res.data.token);
        cookie.save('isAuthenticated', true);
        dispatch({
          type: 'LOGIN_SUCCESS',
          payload: res.data
        });
        history.push('/');
      }
      if (res.data.error) {
        dispatch({
          type: 'LOGIN_FAIL',
          payload: res.data
        });
      }
    })
    .catch(err => {
      dispatch({
        type: 'LOGIN_FAIL',
        payload: err.data
      });
    });
  }
}

export function logout(data) {
  cookie.remove('token');
  cookie.remove('isAuthenticated');
  return {
    type: 'LOGOUT_SUCCESS'
  }
}


export default {
  login,
  logout
};

