import auth from './auth';
import user from './user';

import { combineReducers } from 'redux';

const reducer = combineReducers({
  auth: auth,
  user: user
});

export default reducer;
