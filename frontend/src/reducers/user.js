const initialState = {
  me: [],
  loading: true
};

function user(state = initialState, action = {}) {
  switch (action.type) {
    case 'LOAD_USER':
      return {
        ...state,
        loading: true
      };
    case 'LOAD_USER_SUCCESS':
      return {
        ...state,
        loading: false,
        me: action.payload.data,
      };
    case 'LOAD_USER_FAIL':
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      return state;
  }
}

export default user;
