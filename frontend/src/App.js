import React, { Component } from 'react'
import { Router, Route, Redirect, withRouter, Switch, Link } from 'react-router-dom';
import About from './components/About/About'
import LoginForm from './components/forms/LoginForm'
import PacientHome from './components/pacientes/home'
import { history } from './helpers';
import cookie from 'react-cookies';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as userActions from './actions/user'
import { Navbar, Chip } from 'react-materialize';

const PrivateRoute = ({ component: Component, isAuthenticated, ...rest }) => (
  <Route {...rest} render={props => (
      isAuthenticated
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
  )} />
)

const NavBar = props => (
  <Navbar className='success' left>
      <ul>
        <li><Link to="/">Index</Link></li>
        <li><Link to="/about">About</Link></li>
        <li><Link to="/turnos">Turnos</Link></li>
        <li><Link to="/admin">Data dashboard</Link></li>
        <li><Link
          to={{
            pathname: '/logs',
            search: '?filter=active',
            state: { fromNavBar: true }
            }}
          >Logs</Link>
        </li>
        <li><Chip>{`Bienvenido: ${props.userEmail ? props.userEmail : null}`}</Chip></li>
      </ul>
  </Navbar>
);


class App extends Component {

  constructor(props) {
    super();
    history.listen((location, action) => {});
    this.state = {
      pages: 0,
      limit: 10,
      sortField: 'name',
      sortDirection: 'asc',
      title: 'DocSis'
    };
  }
  componentDidMount() {
    const isAuthenticated = cookie.load('isAuthenticated');
    if (isAuthenticated) {
      const arrayOfPromises = [
        this.props.actions.userActions.loadUserData(),
      ];
      Promise.all(arrayOfPromises).catch(
      ).then(() => {
        console.log('Success load')
      });
    }
  }

  render() {
    const isAuthenticated = cookie.load('isAuthenticated');
    const { user } = this.props;
    console.log(user)
    return (
      <div>
        <div className="container-fluid">
          <div className="col-lg-12">
            <div className="col-md-12">
              <Router history={history}>
                <div>
                  {(() => { 
                    if (isAuthenticated) {
                      return (<NavBar userEmail = {user.me.email}/>)
                    }
                  })()}
                  <Switch>
                  <PrivateRoute exact path="/" component={About}  isAuthenticated={isAuthenticated} />
                  <PrivateRoute exact path="/turnos" component={PacientHome}  isAuthenticated={isAuthenticated} />
                  <Route path="/login" component={LoginForm}/>
                  <Route path="/register" component={LoginForm} />
                  </Switch>
                  </div>
                </Router>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      userActions: bindActionCreators(userActions, dispatch)
    }
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))