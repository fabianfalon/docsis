import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Button, Row, Input } from 'react-materialize';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as authActions from '../../actions/auth'

class LoginForm extends Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      email: '',
      password: '',
      submitted: false
    }
  }

  handleChange(value, event) {
    this.setState({ [value]: event.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { email, password } = this.state;
    if (email && password) {
      this.props.actions.login(email, password);
    }
  }


  render() {
    return (
      <div>
        <center>
          <h1>Login</h1>
          <div style={{ maxWidth: '800px' }} className={'form-group ' + (this.props.auth.error ? 'has-error' : '')}>
            {this.props.auth.error ? (<span className="help-block m-b-none">{this.props.auth.error}</span>) : (<span />)}
            <form name="form" onSubmit={this.handleSubmit}>
              <Row>
                <Input type="email" label="Email" s={12} style={{ fontSize: '23px' }} onChange={(event) => { this.handleChange('email', event); }} />
                <Input type="password" label="password" s={12} style={{ fontSize: '23px' }} onChange={(event) => { this.handleChange('password', event); }} />
              </Row>
              <Row>
                <Button waves='light' className='blue'
                  onClick={this.handleSubmit}>Aceptar</Button>
                {this.props.auth.token &&
                  <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                }
                &nbsp; &nbsp;
              <Button waves='light' className='red'>Cancelar</Button>
              </Row>
            </form>
          </div>
        </center>
      </div>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    auth: state.auth,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch)
  }
}

export default (connect(mapStateToProps, mapDispatchToProps)(LoginForm))
