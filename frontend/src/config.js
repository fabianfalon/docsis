const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || '127.0.0.1',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || '127.0.0.1',
  apiPort: process.env.APIPORT || 5000,
  app: {
    title: 'DocSis',
    description: 'Toda tu información en un solo lugar.',
    head: {
      titleTemplate: 'DocSis',
      meta: [
        { name: 'description', content: 'Toda tu información en un solo lugar.' },
        { charset: 'utf-8' },
      ]
    }
  },

}, environment);